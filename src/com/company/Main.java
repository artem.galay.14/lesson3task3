package com.company;

/**
 * Задание 3. Доработать классы MathBox и ObjectBox таким образом, чтобы MathBox был наследником ObjectBox.
 * Необходимо сделать такую связь, правильно распределить поля и методы.
 * Функциональность в целом должна сохраниться.
 * При попытке положить Object в MathBox должно создаваться исключение.
 */

public class Main {

    public static void main(String[] args) {
	// write your code here
        // ObjectBox Block
        System.out.println("Функционал ObjectBox");
        ObjectBox m = new ObjectBox(new Number[]{1,2,3,4,5,6,7,8,9,10,10,11,5});
        System.out.println("Получили список: " + m.dump());

        m.addObject(100);
        System.out.println("Добавили 100: " + m);

        m.deleteObject(2); //удаляем элемент "2"
        System.out.println("Удалили элементы 2: " + m);

        System.out.println();

        // MathBox Block
        System.out.println("Функционал MathBox");
        MathBox m1 = new MathBox(new Number[]{1,2,3,4,5,6,7,8,9,10,10,11,5});
        System.out.println("Получили список: " + m1);
        System.out.println("hashCode: " + m.hashCode());

        MathBox m2 = new MathBox(new Number[]{1,2,3,4,5,6,7,8,9,10,10,11,5}); //второй такой же
        System.out.println("Получили второй список: " + m1);
        System.out.println("Проверяем equals: " + m1.equals(m2));

        System.out.println("summat: " + m1.summat());
        System.out.println("Разделили элементы на 2: " + m1.splitter(2));

        m1.delE(2); //удаляем элемент "2"
        System.out.println("Удалили элементы 2: " + m1);

        //При попытке положить Object в MathBox должно создаваться исключение.
        //MathBox m3 = new MathBox(new Object[10]);
    }
}
