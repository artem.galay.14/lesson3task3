package com.company;

/**
 * My custom exception class.
 */
class AlsCustomException extends Exception
{
    public AlsCustomException(String message)
    {
        super(message);
    }
}