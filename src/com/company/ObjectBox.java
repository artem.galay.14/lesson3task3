package com.company;

/**
 * Задание 2. Создать класс ObjectBox, который будет хранить коллекцию Object.
 * <p>
 * У класса должен быть метод addObject, добавляющий объект в коллекцию.
 * У класса должен быть метод deleteObject, проверяющий наличие объекта в коллекции и при наличии удаляющий его.
 * Должен быть метод dump, выводящий содержимое коллекции в строку.
 */

import java.util.*;

public class ObjectBox<T> {
    private Set<T> setNum; //список на выход

    // Generic function to convert an Array to list
    private static <T> Set<T> convertArrayToSet(T array[]) {
        // Create the list by passing the Array
        // as parameter in the constructor
        Set<T> set = new HashSet<>(Arrays.asList(array));

        // Return the converted Set
        return set;
    }

    // Конструктор на вход получает массив Number. Элементы не могут повторяться.
    // Элементы массива внутри объекта раскладываются в подходящую коллекцию (выбрать самостоятельно).
    public ObjectBox(T[] arrayNum) {
        setNum = convertArrayToSet(arrayNum);
    }

    @Override
    public String toString() {
        return this.dump();
    }

    @Override
    public int hashCode() {
        return setNum.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        ObjectBox obj2 = (ObjectBox) obj;
        return obj2.setNum.equals(this.setNum); // doc:  Returns true if and only if the specified object is also a list, both lists have the same size, and all corresponding pairs of elements in the two lists are equal.
    }

    public Set<T> getSetNum() {
        return setNum;
    }

    // У класса должен быть метод addObject, добавляющий объект в коллекцию.
    public void addObject(T e) {
        setNum.add(e);
    }


    //  У класса должен быть метод deleteObject, проверяющий наличие объекта в коллекции и при наличии удаляющий его.
    public void deleteObject(T e) {
        Iterator<T> it = setNum.iterator();
        while (it.hasNext()) {
            T n = it.next();
            if (n == e) {
                it.remove();
            }
        }
    }

    //Должен быть метод dump, выводящий содержимое коллекции в строку.
    public String dump() {
        return setNum.toString();
    }

}
