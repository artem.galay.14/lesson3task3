package com.company;

import java.io.IOException;
import java.util.*;

public class MathBox extends ObjectBox<Number> {
    // Конструктор на вход получает массив Number. Элементы не могут повторяться.
    // Элементы массива внутри объекта раскладываются в подходящую коллекцию (выбрать самостоятельно).

    public MathBox(Number[] arrayNum) {
        super(arrayNum);
        try {
            //НАСТАВНИК
            // вам ничего не мешает отследить это на этапе компиляции, параметризовав ObjectBox Number-ом при объявлении класса MathBox
            // Артём
            // Именно так и сделал изначально. Но в условии задачи сказано, что должно именно создаваться исключение.
            // Я решил, что это приницпиальный момент, и пришлось "извращаться" (:
            // Добавил параметр в объявлении
            if (arrayNum[0] instanceof Number != true) {
                throw new AlsCustomException("Exception: Подали не число");
            }
            //НАСТАВНИК
            // помните, на этот момент у вас уже есть в listNum все данные из arrayNum. Этот цикл фактически бесполезен, так как условие в if всегда будет false
            // в результате вы не выполняете требование, что элементы не должны повторяться
            // Артём
            // переписал через Set родительского
        } catch (AlsCustomException e) {
            System.out.println(e.getMessage());
        }
    }

    //Существует метод summator, возвращающий сумму всех элементов коллекции.
    public double summat() {
        double sum = 0d;
        for (Number b : getSetNum()) {
            //НАСТАВНИК
            // зачем два преобразования?
            // Артём
            // сделал через doubleValue
            sum = sum + b.doubleValue();
        }
        return sum;
    }

    //Существует метод splitter, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода.
    // Хранящиеся в объекте данные полностью заменяются результатами деления.
    public MathBox splitter(int div) {
        Set<Number> newSetNum = new HashSet<>();
        for (Number b : getSetNum()) {
            newSetNum.add((int) b / div);
        }
        Number[] newArray = newSetNum.toArray(new Number[0]);
        MathBox newMathBox = new MathBox(newArray);
        return newMathBox;
    }

    //  Создать метод, который получает на вход Integer и если такое значение есть в коллекции, удаляет его.
    public void delE(Number e) {
        deleteObject(e);
    }

}
